import requests
import json
import mysql.connector
from datetime import datetime

# Criptomonedas que deseas obtener los precios
criptos = ['bitcoin', 'ethereum', 'litecoin','waves','hive','tezos','nym','cardano','ontology','monero']
criptos_parametrizada = {'bitcoin':'BTC', 'ethereum':'ETH', 'litecoin':'LTC','waves':'WAVES','hive':'HIVE','tezos':'XTZ','nym':'NYM','cardano':'ADA','ontology':'ONT','monero':'XMR'}

# Obtener fecha y hora actual
now = datetime.now()
fecha_actual = now.strftime('%Y-%m-%d %H:%M:%S')

# Obtener precios de las criptomonedas
criptos_str = ','.join(criptos) # Convertir la lista de criptos a una cadena separada por comas
url = f'https://api.coingecko.com/api/v3/simple/price?ids={criptos_str}&vs_currencies=usd'
response = requests.get(url)
data = json.loads(response.text)

# Obtener precios y guardarlos en una lista de tuplas
precios = []
for cripto in criptos:
    precio = data[cripto]['usd']
    precios.append((criptos_parametrizada[cripto], precio))

# Conexión a la base de datos
cnx = mysql.connector.connect(user='root', password='geocom',
                              host='localhost', database='price_history')
cursor = cnx.cursor()

# Insertar fecha en la tabla 'fechas'
query = "INSERT INTO fechas (fecha,fuente) VALUES (%s,%s)"
values = (fecha_actual,'coingecko')
cursor.execute(query, values)
id_fecha = cursor.lastrowid  # Obtener el id de la nueva fecha

# Insertar precios en la tabla 'precios'
query = "INSERT INTO precios (id_fecha, criptomoneda, precio) VALUES (%s, %s, %s)"
values = [(id_fecha, cripto, precio) for cripto, precio in precios]
cursor.executemany(query, values)

# Guardar cambios en la base de datos y cerrar la conexión
cnx.commit()
cursor.close()
cnx.close()
