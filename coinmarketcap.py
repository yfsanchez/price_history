import requests
import json
import mysql.connector
from datetime import datetime
import json

def coinmarketcap_apikey():
    with open('api_key.json') as f:
        config = json.load(f)
        return config['coinmarketcap']['api_key']


# Criptomonedas que deseas obtener los precios
#criptos = ['bitcoin', 'ethereum', 'litecoin','waves','hive-blockchain','tezos','nym','cardano','ontology']
#criptos = ['bitcoin']
criptos = ['BTC', 'ETH','LTC', 'WAVES','XTZ','ONT','NYM','HIVE','ADA','XMR']

# Obtener fecha y hora actual
now = datetime.now()
fecha_actual = now.strftime('%Y-%m-%d %H:%M:%S')

# Obtener precios de las criptomonedas desde CoinMarketCap
api_key = coinmarketcap_apikey()
headers = {
  'Accepts': 'application/json',
  'X-CMC_PRO_API_KEY': api_key
}
url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest'
params = {'symbol': ','.join(criptos), 'convert': 'USD'}
response = requests.get(url, headers=headers, params=params)
data = response.json()


# Obtener precios y guardarlos en una lista de tuplas
precios = []
for cripto in criptos:
    precio = data['data'][cripto]['quote']['USD']['price']
    precios.append((cripto, precio))

# Conexión a la base de datos
cnx = mysql.connector.connect(user='root', password='geocom',
                              host='localhost', database='price_history')
cursor = cnx.cursor()

# Insertar fecha en la tabla 'fechas'
query = "INSERT INTO fechas (fecha, fuente) VALUES (%s, %s)"
values = (fecha_actual, 'coinmarketcap')
cursor.execute(query, values)
id_fecha = cursor.lastrowid  # Obtener el id de la nueva fecha

# Insertar precios en la tabla 'precios'
query = "INSERT INTO precios (id_fecha, criptomoneda, precio) VALUES (%s, %s, %s)"
values = [(id_fecha, cripto, precio) for cripto, precio in precios]
cursor.executemany(query, values)

# Guardar cambios en la base de datos y cerrar la conexión
cnx.commit()
cursor.close()
cnx.close()
