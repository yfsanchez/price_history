import requests
import json
import mysql.connector
from datetime import datetime
import json

def coinmarketcap_apikey():
    with open('api_key.json') as f:
        config = json.load(f)
        return config['coinmarketcap']['api_key']

# Criptomonedas que deseas obtener los precios
#criptos = ['bitcoin', 'ethereum', 'litecoin','waves','hive-blockchain','tezos','nym','cardano','ontology']
#criptos = ['BITCOIN', 'ETHEREUM']
criptos = ['BITCOIN']

# Obtener fecha y hora actual
now = datetime.now()
fecha_actual = now.strftime('%Y-%m-%d %H:%M:%S')

# Obtener precios de las criptomonedas desde CoinMarketCap
api_key = coinmarketcap_apikey()
url = f'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?symbol={",".join(criptos)}&convert=USD'
headers = {'X-CMC_PRO_API_KEY': api_key}
response = requests.get(url, headers=headers)
data = json.loads(response.text)['data']

print(data)

# Obtener precios y guardarlos en una lista de tuplas
precios = []
for cripto in criptos:
    precio = data[cripto]['quote']['USD']['price']
    precios.append((cripto, precio))
print(precios)