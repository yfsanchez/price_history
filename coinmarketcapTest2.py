import requests

# Criptomonedas que deseas obtener los precios
criptos = ['BTC', 'LTC', 'WAVES','XTZ','ONT','NYM','HIVE','ADA']

# Obtener precios de las criptomonedas
headers = {
  'Accepts': 'application/json',
  'X-CMC_PRO_API_KEY': 'api-key'
}
url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest'
params = {'symbol': ','.join(criptos), 'convert': 'USD'}
response = requests.get(url, headers=headers, params=params)
data = response.json()

# Obtener precios y guardarlos en una lista de tuplas
precios = []
for cripto in criptos:
    precio = data['data'][cripto]['quote']['USD']['price']
    precios.append((cripto, precio))

# Imprimir los precios
for cripto, precio in precios:
    print(f'El precio de {cripto} es {precio} USD')
