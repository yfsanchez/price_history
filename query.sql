SELECT * FROM fechas order by fecha desc limit 10
;
SELECT * FROM precios order by id_fecha desc limit 30
;

CREATE TABLE fechas  ( 
	id    	int(11) AUTO_INCREMENT NOT NULL,
	fecha 	datetime NOT NULL,
	fuente	text NULL,
	PRIMARY KEY(id)
)
GO
CREATE TABLE precios  ( 
	id          	int(11) AUTO_INCREMENT NOT NULL,
	id_fecha    	int(11) NOT NULL,
	criptomoneda	varchar(50) NOT NULL,
	precio      	decimal(18,10) NOT NULL,
	PRIMARY KEY(id)
)
GO
ALTER TABLE precios
	ADD CONSTRAINT fk_precios_fechas
	FOREIGN KEY(id_fecha)
	REFERENCES fechas(id)
	ON DELETE RESTRICT 
	ON UPDATE RESTRICT 
GO


