import requests

url = 'https://api.coingecko.com/api/v3/simple/price?ids=bitcoin&vs_currencies=usd'
response = requests.get(url)

if response.status_code == 200:
    data = response.json()
    price = data['bitcoin']['usd']
    print(f"El precio de Bitcoin es de {price} USD.")
else:
    print("Error al obtener el precio de Bitcoin.")
