import requests
import json

# Definir la criptomoneda y la moneda de cotización (en este caso, BTC a USD)
crypto = 'bitcoin,ethereum,litecoin'
vs_currency = 'usd'

# Hacer la solicitud GET a la API de CoinGecko
response = requests.get(f'https://api.coingecko.com/api/v3/simple/price?ids={crypto}&vs_currencies={vs_currency}')

# Obtener los datos de la respuesta en formato JSON
data = json.loads(response.text)
print(data)

# Obtener el precio de la criptomoneda en USD
#price = data[crypto][vs_currency]

# Imprimir el precio
#print(f'El precio de {crypto.upper()} es de {price} {vs_currency.upper()}.')
