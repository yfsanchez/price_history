import requests
import json
import mysql.connector
from datetime import datetime

# Obtener fecha y hora actual
now = datetime.now()
fecha_actual = now.strftime('%Y-%m-%d %H:%M:%S')

# Obtener precios de las criptomonedas
url = 'https://api.coingecko.com/api/v3/simple/price?ids=bitcoin%2Cethereum%2Clitecoin&vs_currencies=usd'
response = requests.get(url)
data = json.loads(response.text)

bitcoin_price = data['bitcoin']['usd']
ethereum_price = data['ethereum']['usd']
litecoin_price = data['litecoin']['usd']

# Conexión a la base de datos
cnx = mysql.connector.connect(user='root', password='geocom',
                              host='localhost', database='price_history')
cursor = cnx.cursor()

# Insertar fecha en la tabla 'fechas'
query = "INSERT INTO fechas (fecha) VALUES (%s)"
values = (fecha_actual,)
cursor.execute(query, values)
id_fecha = cursor.lastrowid  # Obtener el id de la nueva fecha

# Insertar precios en la tabla 'precios'
query = "INSERT INTO precios (id_fecha, criptomoneda, precio) VALUES (%s, %s, %s)"
values = [(id_fecha, 'bitcoin', bitcoin_price),
          (id_fecha, 'ethereum', ethereum_price),
          (id_fecha, 'litecoin', litecoin_price)]
cursor.executemany(query, values)

# Guardar cambios en la base de datos y cerrar la conexión
cnx.commit()
cursor.close()
cnx.close()
